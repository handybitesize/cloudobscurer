var mongo   = require('mongodb'),
	url     = require('url'),
    config  = require('./../config.'+process.env.NODE_ENV+'.json');
    swig    = require('swig');
	
var MongoClient = require('mongodb').MongoClient;
var BSON = mongo.BSONPure;
var mongoUri = config.mongouri;
MongoClient.connect(mongoUri, function(err, db) {
     if(!err) {
        db.collection('hashlookup', {strict:true}, function(err, collection) {
            if (err) {
                console.log("The 'hashlookup' collection doesn't exist. Creating a sample");
               // populateDB();
            }
        });
    }else{

        console.log("Database connection error");
    }
}); 


exports.findAll = function(req, res) {
    var tpl = swig.compileFile(__dirname+'/../ui/home.htm');
    var files = [];
    MongoClient.connect(mongoUri, function(err, db) {
        if(!err) {
            db.collection('hashlookup', function(err, collection) {
                collection.find().toArray(function(err, items) {
      		        for (var i = 0; i < items.length; i++) {
  			           x = items[i];
  			           str = "<a href='/" + x.p + "'>" + x.p + "</a> (Viewed "+ x.c +" times)<br/>";
                        x.deletekey = String(x._id);
                        files.push(x);
			         }           
                    res.writeHead(200, {"Content-Type": "text/html"}); 
                    res.write(tpl({ files: files, pagename: 'Sample Files', mode: process.env.NODE_ENV }))
                    res.end();
                });
            });
        }else{
            console.log("Database getAll error");
        }    
    });
};

 

exports.findByPath = function(req, res) {
	var p = req.params.p;
	if (!p) p = req.params[0];
	console.log('Retrieving file: ' + p);
    MongoClient.connect(mongoUri, function(err, db) {
        if(!err) {
        	db.collection('hashlookup', function(err, collection) {
        	collection.findAndModify(
                {'p': p},  // query
               [], // no sort order
                {$inc: {'c': 1}}, // increments field "c"
                {fields: {"u":1}}, // options
                function(err, item) {
                	protocol = url.parse(item.u).protocol;
        			if (protocol == 'http:') fileGetter = require('http');
        			else fileGetter = require('https');
        			fileGetter.get(item.u, function(proxyRes) {proxyRes.pipe(res);});
                });
        	});
        }else{
                console.log("Database fetch error");
        }    
    });
};


exports.addFile = function(req, res) {
    var file = req.body;
    file.c = 0;
    console.log('Adding file: ' + JSON.stringify(file));
    MongoClient.connect(mongoUri, function(err, db) {
        if(!err) {
            db.collection('hashlookup', function(err, collection) {
                collection.insert(file, {safe:true}, function(err, result) {
                    if (err) {
                        res.send({'error':'An error has occurred'});
                    } else {
                        console.log('Success: ' + JSON.stringify(result[0]));
                        res.redirect('/');
                    }
                });
            });
        }else{
             console.log("Database insert error");
        }
    });
};

exports.updateFile = function(req, res) {
    var p = req.params.p;
    var file = req.body;
    console.log('Updating file: ' + p);
    console.log(JSON.stringify(file));
    MongoClient.connect(mongoUri, function(err, db) {
        if(!err) {
            db.collection('hashlookup', function(err, collection) {
                collection.update({'p': p}, file, {safe:true}, function(err, result) {
                    if (err) {
                        console.log('Error updating file: ' + err);
                        res.send({'error':'An error has occurred'});
                    } else {
                        console.log('' + result + ' document(s) updated');
                        res.send(file);
                    }
                });
            });
        }else{
                 console.log("Database update error");
        }
    });
};

exports.deleteFile = function(req, res) {
	console.log('starting delete')
    var id = req.params.id;
    console.log('Deleting file: ' + id);
    MongoClient.connect(mongoUri, function(err, db) {
       if(!err) { 
            db.collection('hashlookup', function(err, collection) {
               collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
                    if (err) {
                        res.send({'error':'An error has occurred - ' + err});
                    } else {
                        console.log('' + result + ' document(s) deleted');
                        res.redirect('/');
                    }
                });
            });
        }else{
            console.log("Database delete error");
        }
    });
};





var populateDB = function() {
 
    var hashlookup = [
    {
        p: "Twig.pdf",
        u: "http://4eaf72920c545f9e9bce-7bf7fb6420674ae5874d8b100c2452c5.r13.cf3.rackcdn.com/Twig.pdf",
        c: 0
    },
    {
        p: "composer.phar",
        u: "http://4eaf72920c545f9e9bce-7bf7fb6420674ae5874d8b100c2452c5.r13.cf3.rackcdn.com/composer.phar",
        c: 0
    },
        {
        p: "rs_logo.jpg",
        u: "http://4eaf72920c545f9e9bce-7bf7fb6420674ae5874d8b100c2452c5.r13.cf3.rackcdn.com/rs_logo.jpg",
        c: 0
    }];


    var rackspacesample = [
    {
        p: "pdfs/CloudFilesDataSheet.pdf",
        u: "http://c1776742.cdn.cloudfiles.rackspacecloud.com/downloads/pdfs/CloudFilesDataSheet.pdf",
        c: 0
    },
        {
        p: "pdfs/CloudFilesSwiftDevelopers.pdf",
        u: "http://c1776742.cdn.cloudfiles.rackspacecloud.com/downloads/pdfs/CloudFilesSwiftDevelopers.pdf",
        c: 0
    },
        {
        p: "pdfs/CaseStudy_Prepara.pdf",
        u: "http://c1776742.cdn.cloudfiles.rackspacecloud.com/downloads/pdfs/CaseStudy_Prepara.pdf",
        c: 0
    },
     
        {
        p: "pdfs/CaseStudy_AgencyNet.pdf",
        u: "http://c1776742.cdn.cloudfiles.rackspacecloud.com/downloads/pdfs/CaseStudy_AgencyNet.pdf",
        c: 0
       },
        {
        p: "pdfs/CaseStudy_LiveSmartSolutions.pdf",
        u: "http://c1776742.cdn.cloudfiles.rackspacecloud.com/downloads/pdfs/CaseStudy_LiveSmartSolutions.pdf",
        c: 0
       },
        {
        p: "open-is-better.jpg",
        u: "http://images.cdn.rackspace.com/home/bananers/open-is-better.jpg",
        c: 0 
       },
        {
        p: "managed-cloud.jpg",
        u: "http://images.cdn.rackspace.com/home/bananers/managed-cloud.jpg",
        c: 0
       },
        {
        p: "cloud-media.jpg",
        u: "http://images.cdn.rackspace.com/home/bananers/cloud-media.jpg",
        c: 0
       },
        {
        p: "hybrid-cloud-1.jpg",
        u: "http://images.cdn.rackspace.com/home/bananers/hybrid-cloud-1.jpg",
        c: 0
    },     
        {
        p: "regular-vs-managed.png",
        u: "http://images.cdn.rackspace.com/cloud/managed_cloud/regular-vs-managed.png",
        c: 0
    }];
 
    db.collection('hashlookup', function(err, collection) {
        collection.insert(rackspacesample, {safe:true}, function(err, result) {});
    });
 
};