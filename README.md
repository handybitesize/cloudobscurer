Cloud Obscure
=========

A simple node.js and mongodb obscurer for serving cloud files invisibily. 

###  Adding routes

```
POST / {"u": 'http://domain.com/fullpath/to/the/remote/file.ext': "p": 'thelocalpath.ext', "c" : 0}
```
Where

 * u = The full resolved path to the remote file
 * p = the local path you would like the file to be served at
 * c = A count of the number of times the file has been accessed

###  Serving files

```
GET /p
```
Where

 * p = the local path added

This should then seemlessly serve the binary without the client having any awareness of its remote nature. 

###  Admin

A simple administration interface is provided by default at 

```
GET /
```



