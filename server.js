var express 	= require('express'),
	files 		= require('./routes/files'),
	config 		= require('./config.'+process.env.NODE_ENV+'.json');
 
var app = express();

app.configure(function () {
    app.use(express.logger('default'));     /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser());
});

console.log(process.env.NODE_ENV);
 
 // Index - not for prod
app.get('/', files.findAll);

//this is annoying favicon handling
app.get('/favicon.ico', function (req,res) { 
	res.send(200);
}
); 

app.get('/delete/:id', files.deleteFile);
//I'm sure the following 2 routes can be combined, first one
// is for /file.ext, the secopnd for /path/to/file.ext
app.get('/:p', files.findByPath); 
app.get(/^\/(.*)/, files.findByPath);

//Simple Crud
app.post('/', files.addFile);
app.put('/:p', files.updateFile);
app.delete('/:id', files.deleteFile);

 
var port = process.env.PORT || config.localport;
app.listen(port, function() {
  console.log("Listening on " + port);
});